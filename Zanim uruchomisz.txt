

Uwagi ogólne:
	Baza jest wystawiona na zewnątrz, zatem nie potrzeba specjalnie instalować MySQLa.
	Do przeglądania stron zalecana jest nowa przegladarka (ze wzgledu na użycie CSS3)
	zainstalowana na komputerze lub telefonie.


Dostęp do kont:

admin: login: admin, password, admin1
klient: login: krzys, password: krzys1

Stronkę możemy uruchomić na 3 sposoby:

1. Przez www: 
Link: 	rexor.pl:8080/PK_GrandRentAuto/
Opis: 	Zwyczajna strona www, nic nie trzeba dodawać.
		Można ją otworzyć na komórce (Responsive Web Design).

2. Netbeans/Eclipse:
Opis:	Kopiujemy repo projektu (https://bitbucket.org/komponenty/project-g-2) a następnie importujemy do Netbeansa.
		Maven pobierze potrzebne biblioteki niezbędne do uruchomienia projektu. 
		Następnie odpalamy projekt.
Uwagi:	Możliwe, że do uruchomienia konieczny będzie tomcat serwer. 
		(Jeśli ktoś posiada instalkę netbeansa, ma tam opcję jego zainstalowania). 

3. Odpalając za pomocą run.bat lub run.sh:
Opis:	(Wymaga uprzednio zbudowania projektu)
		Wystarczy uruchomić z konsoli plik run.bat (windows) lub run.sh (linux). 
		Po wyświetleniu komunikatu w konsoli, o gotowości tomcata, wystarczy 
		otworzyć przegląrakę i w adresie wpisać: 
		localhost:8080//GrandRentAuto/
Uwagi:	Jeżeli konsola zostanie wyłączona bądź tomcat nie zainicjalizuje sie w pełni, stronka NIE ODPALI SIE.
		Błąd 500 przy próbie wysłania maila: Wysyłka eamil może nie działać, jeżeli program antywirusowy blokuje połączenia TSL.
		Błąd 500 przy generowaniu xml: plik ustawiony jest domyślnie na 'read-only'.