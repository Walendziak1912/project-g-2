//	Detect device (mobile/desktop)
var clickEvent;
if(Modernizr.touch){
	clickEvent = "touchstart";
} else if(window.navigator.msPointerEnabled){
	clickEvent = "MSPointerDown";
} else {
	clickEvent = "click";
}

$(function(){
	//	Detect and optimize font-size for Symbian OS users
	if (/SymbianOS/.test(window.navigator.userAgent) ||
		/Symbian/.test(window.navigator.userAgent)){
		$("body").css({
			"font-size":"2.3em"
		});
		$("li").css({
			"font-size":"120%"
		});
	}
	
	//	Init menu
	$animatedMenu = $("button.animated");
	$animatedMenu.on(clickEvent, function(){
		showResult(clickEvent + ", window size:" + $(document).width() + ": " + $(document).height());
		animateMenu($(this));
	});
	
	//	add listener to set container width and height depend on window size
	resizeContent();
	window.onresize = function(){	resizeContent();	};
	
	//	Detect slider arrow clicks
	var currentIndex = 0;
	$arrowLeft = $(".arrow.left");
	$arrowRight = $(".arrow.right");
	$arrowRight.on(clickEvent, function(){
		nextSlide(++currentIndex);
	});
	$arrowLeft.on(clickEvent, function(){
		nextSlide(--currentIndex);
	});
	
	setInterval(function(){ nextSlide(++currentIndex) }, 5000);
});

	
//	To show information on phone
function showResult(string){
//	$(".info").html(string);
}


//	Menu animation
function animateMenu($element){
	$target = $($element.data("target"));
	$className = $element.data("toggle");
	if($target.hasClass($className)){
		$target.removeClass($className);
		$target.css({
			"height": 0 + "px"
		});
	} else {
		$target.addClass($className);
		$menuHeight = $target.children(".nav").height();
		$target.css({
			"height": $menuHeight + "px"
		});
	}
}


//	Resize main content(slider) depend on desktop and other element height (safe version for cross-browser)
function resizeContent(){
	$containerHeight = $(window).height()- $("header").height() - $("footer").height();
	$containerMinHeight = parseInt($("#container").css("min-height"));
	if($containerHeight <= $containerMinHeight) {	$containerHeight = $containerMinHeight;	}
	$("#container").height($containerHeight);
	showResult("resized to "+ $(window).width() + ":" + $containerHeight);
	fitImage();
}


//	Main slider functions
function fitImage(){
	if(Modernizr.mq("(max-width: 767px)")){
		$fittedHeight = $("#slider").height() - $("#slider .description").height();
		$("#slider .picture").height($fittedHeight);
		showResult(" height changed to " + $fittedHeight);
	}
}

function nextSlide(index){
	var slidesNum = $("#slider .slide").length;
	
//	console.log(index + " % " + slidesNum + " = " + index % slidesNum);
	
	if(index < 0){
		index = slidesNum - Math.abs(index) % slidesNum;
	} else if(index >= slidesNum){
		index = index % slidesNum;
	}
	
	showResult("current index is: " + index);
	var posX = (index * 100);
	
//	console.log(posX);
	$("#slide-cont").css({
		"left": -posX + '%'
	});
}
