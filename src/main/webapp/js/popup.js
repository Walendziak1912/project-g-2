var clickEvent;
if(Modernizr.touch){
	clickEvent = "touchstart";
} else if(window.navigator.msPointerEnabled){
	clickEvent = "MSPointerDown";
} else {
	clickEvent = "click";
}

$(function(){
	$('.edit_btn').each(function(){
		this.on(clickEvent, function(){
			showPopup();
		});
	});
});

function showPopup(){
	$('#popup').css({
		'z-index': 9999,
		'opacity': 1
	});
}

function hidePopup(){
	$('#popup').css({
		'z-index': -9999,
		'opacity': 0
	});
}