var clickEvent;
if(Modernizr.touch){
	clickEvent = "touchstart";
} else if(window.navigator.msPointerEnabled){
	clickEvent = "MSPointerDown";
} else {
	clickEvent = "click";
}

$(function(){
	$('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' });
});