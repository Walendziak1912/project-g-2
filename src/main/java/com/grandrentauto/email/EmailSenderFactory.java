package com.grandrentauto.email;

public class EmailSenderFactory {
    public static EmailSender createEmailSender(String login, String password){
        return new EmailSenderImpl(login, password);
    }
}
