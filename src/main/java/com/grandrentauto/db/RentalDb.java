package com.grandrentauto.db;

import com.grandrentauto.data.Car;
import com.grandrentauto.data.Client;
import com.grandrentauto.data.Rental;
import static com.grandrentauto.db.Database.conn;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;


@Service
public class RentalDb extends Database<Rental>{

    @Override
    public Rental getRow(ResultSet rs) throws Exception{
        CarDb carDb = new CarDb();
        ClientDb clientDb = new ClientDb();
        Car car = carDb.getCar(rs.getInt("idcar"));
        Client client = clientDb.getClient(rs.getInt("idclient"));
        return new Rental(rs.getInt("id"),car, client, rs.getDate("start"), rs.getDate("end"));
    }
    
    public void saveRental(Integer clientId, Integer carId, java.util.Date start, java.util.Date end) throws Exception{
        String sql = "INSERT INTO rental(idcar,idclient,START,END) VALUES (?,?,?,?)";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setInt(1, carId);
        pstmt.setInt(2, clientId);
        pstmt.setDate(3, new Date(start.getTime()));
        pstmt.setDate(4, new Date(end.getTime()));
        pstmt.execute();
    }

    public void updateRental(Integer rentalId, Integer clientId, Integer carId, java.util.Date start, java.util.Date end) throws Exception{
        String sql = "UPDATE rental SET idcar = ?, idclient = ?, START = ?, END = ? where id = ?";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setInt(1, carId);
        pstmt.setInt(2, clientId);
        pstmt.setDate(3, new Date(start.getTime()));
        pstmt.setDate(4, new Date(end.getTime()));
        pstmt.setInt(5,rentalId);
        pstmt.execute();
    }
    
    public void deleteRental(Integer rentalId) throws Exception{
        String sql = "DELETE FROM rental where id = ?";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setInt(1, rentalId);
        pstmt.execute();
    }
    
    public List<Rental> getRental() throws Exception {
        String sql = "Select * from rental";
        return getList(sql);
    }

    public List<Car> searchFreeCars(java.util.Date startDate, java.util.Date endDate) throws Exception {
        return new CarDb().getAvailableCars(startDate,endDate);
        
    }
}
