package com.grandrentauto.db;

import com.grandrentauto.data.Client;
import static com.grandrentauto.db.Database.conn;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class ClientDb extends Database<Client>{

    @Override
    public Client getRow(ResultSet rs) throws Exception {
        Client c =  new Client(rs.getInt("idclient"), rs.getString("first_Name"), rs.getString("last_Name"), rs.getString("address"),rs.getString("phone"),rs.getString("type"));
        c.setLogin(rs.getString("login"));
        c.setPassword(rs.getString("password"));
        c.setUserType(rs.getString("type"));
        return c;
    }
    
    public Client checkUser(String login, String password){
        String sql = "SELECT idclient,first_name,last_name,address,phone, login, password, type from client where login = ? and password = ?";
        try{
            Client result = null;
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, login);
            pstmt.setString(2, password);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
                result = getRow(rs);
            }
            return result;
        }
        catch(Exception ex){
            return null;
        }
    }
    
    public List<Client> getClients(){
        String sql = "SELECT idclient,first_name,last_name,address,phone, login, password, type from client";
        try{
        return getList(sql);
        }
        catch(Exception ex){
            return new ArrayList<Client>();
        }
    }
    public void saveClient(Client client) throws Exception{
       String sql = "INSERT INTO client(first_name,last_name,address,phone,login,password,type) VALUES(?,?,?,?,?,?,?)";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, client.getFirstName());
        pstmt.setString(2, client.getLastName());
        pstmt.setString(3, client.getAddress());
        pstmt.setString(4, client.getPhone());
        pstmt.setString(5, client.getLogin());
        pstmt.setString(6, client.getPassword());
        pstmt.setString(7, client.getUserType());
        pstmt.execute();
    }

    Client getClient(int clientId) {
         String sql = "SELECT idclient,first_name,last_name,address,phone, login, password, type from client where idclient = " + clientId;
        try {
            return getList(sql).get(0);
        } catch (Exception ex) {

        }
        return null;
    }

    public void updateClient(Integer id, String firstName, String lastName, String address, String phone, String login, String password, String userType) throws Exception{
        String sql = "UPDATE client SET first_name = ?, last_name = ?, address = ?, phone = ?, login = ?, password = ?, type = ? where idclient = ?";
         PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, firstName);
        pstmt.setString(2, lastName);
        pstmt.setString(3, address);
        pstmt.setString(4, phone);
        
        pstmt.setString(5, login);
        pstmt.setString(6, password);
        pstmt.setString(7,userType);
        pstmt.setInt(8, id);
        pstmt.execute();
    }

    public void removeClient(Integer id) throws Exception{
        String sql = "DELETE from client where idclient = " + id;
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.execute();
    }

}
