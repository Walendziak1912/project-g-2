package com.grandrentauto.db;

import java.sql.ResultSet;
import com.grandrentauto.webbeans.UserBean;

public class UserDb extends Database<UserBean>{
    

    @Override
    protected UserBean getRow(ResultSet rs) throws Exception {
        return new UserBean();
    }
}
