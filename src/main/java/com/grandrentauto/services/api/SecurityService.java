package com.grandrentauto.services.api;

public interface SecurityService{ 
    public boolean login(String login, String password);
}
