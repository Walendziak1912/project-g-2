package com.grandrentauto.webbeans;

import com.grandrentauto.data.Car;
import com.grandrentauto.db.RentalDb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@ManagedBean
@ViewScoped
public class CustomerRental implements Serializable{
    
    @ManagedProperty(value="#{userBean}")
    private UserBean userBean;
    
    private List<Car> cars;
    
    private String carId;
    private Date startDate;
    private Date endDate;

    @Autowired
    RentalDb rentalDb;
    
    public CustomerRental(){
        cars = new ArrayList<Car>();
    }
    
    public String makeReservation(Car c, int clientId) throws Exception{
        rentalDb.saveRental(clientId, c.getId(), startDate, endDate);
        
        return null;
    }
    
    public String makeReservation(int id) throws Exception{
        rentalDb.saveRental(userBean.getClient().getId(), 123, startDate, endDate);
        
        return null;
    }
    
    public String searchFreeCars() throws Exception{
        List<Car> availableCars = rentalDb.searchFreeCars(startDate,endDate);
        cars = availableCars;
        return null;
    }
    /**
     * @return the cars
     */
    public List<Car> getCars() {
        return cars;
    }

    /**
     * @param cars the cars to set
     */
    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    /**
     * @return the carId
     */
    public String getCarId() {
        return carId;
    }

    /**
     * @param carId the carId to set
     */
    public void setCarId(String carId) {
        this.carId = carId;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the userBean
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * @param userBean the userBean to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }
    
}
