package com.grandrentauto.webbeans;

import com.grandrentauto.data.Client;
import com.grandrentauto.db.ClientDb;
import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@ManagedBean(name="userBean")
@SessionScoped
@Component
public class UserBean implements Serializable{
    private String login;
    private String password;
    private Client client;
    
    @Autowired
    ClientDb clientDb;
    
    public String authenticate(){
        try{
            
        }catch(Throwable ex){
           
        }
        
        setClient(clientDb.checkUser(getLogin(), getPassword()));
        Client c =getClient();
       if(c != null){
           this.client = c;
           return "mainmenu";
       }
       else{
           return null;
       }
    }

    public void hasAccess() throws IOException{
        if(client == null){
            FacesContext.getCurrentInstance().getExternalContext().redirect("Login.xhtml");
        }
    }
    
    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }
}
