package com.grandrentauto.webbeans;

import com.grandrentauto.data.Client;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;


@ManagedBean(name="security")
@RequestScoped
public class SecurityListener {

    @ManagedProperty(value="#{userBean}")
    private UserBean userBean;
    
    
    public void authenticate()  {
        Client c = getUserBean().getClient();
        
        if(c == null){
            FacesContext fc = FacesContext.getCurrentInstance();
            ConfigurableNavigationHandler nav 
		   = (ConfigurableNavigationHandler) 
			fc.getApplication().getNavigationHandler();
 
		nav.performNavigation("login.xhtml");
        }
    }

    /**
     * @return the userBean
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * @param userBean the userBean to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }
    
}
