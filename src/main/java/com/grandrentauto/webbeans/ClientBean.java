package com.grandrentauto.webbeans;

import com.grandrentauto.data.Client;
import com.grandrentauto.db.ClientDb;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@ManagedBean
@RequestScoped
public class ClientBean {
    
    private List<Client> clients;
    
    private String firstName;
    private String lastName;
    private String address;
    private String phone;
    private Integer userId;
    private Boolean toEdit;
    private String userType;
    
    private String password;
    private String login;
    
    @Autowired
    ClientDb clientDb;
    
    public List<Client> getClients(){
        clients = clientDb.getClients();
        return clients;
    }

    public String removeClient(Integer id) throws Exception{
        clientDb.removeClient(id);
        return null;
    }
    
    public String editClient(Integer id){
    
        this.setUserId(id);
        for(Client c : getClients()){
            if(c.getId() == id){
                c.setToEdit(true);
                this.setAddress(c.getAddress());
                this.setFirstName(c.getFirstName());
                this.setLastName(c.getLastName());
                this.setPhone(c.getPhone());
                this.setPassword(c.getPassword());
                this.setLogin(c.getLogin());
                this.setUserType(c.getUserType());
            }
            else{
                c.setToEdit(false);
            }
        }
        return null;
    }
    public String update(Integer id){
        try {
            clientDb.updateClient(id, getFirstName(), getLastName(), getAddress(), getPhone(), getLogin(), getPassword(), getUserType());
        } catch (Exception ex) {
            Logger.getLogger(ClientBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public String addClient(){
        try {
            if(getUserId()!= null && getUserId()>0){
                clientDb.updateClient(getUserId(), getFirstName(), getLastName(), getAddress(), getPhone(), getLogin(), getPassword(), getUserType());
            }
            else{
            clientDb.saveClient(new Client(-1, getFirstName(), getLastName(), getAddress(), getPhone(), getUserType()));
            }
        } catch (Exception ex) {
            Logger.getLogger(ClientBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /**
     * @param clients the clients to set
     */
    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the toEdit
     */
    public Boolean getToEdit() {
        return toEdit;
    }

    /**
     * @param toEdit the toEdit to set
     */
    public void setToEdit(Boolean toEdit) {
        this.toEdit = toEdit;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * @param userType the userType to set
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }
}
