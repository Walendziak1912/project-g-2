package com.grandrentauto.webbeans;

import com.grandrentauto.data.Car;
import com.grandrentauto.data.Client;
import com.grandrentauto.data.Rental;
import com.grandrentauto.db.RentalDb;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xmlGenerator.XMLDocument;
import xmlGenerator.XMLElement;
import xmlGenerator.XMLGenerator;
import xmlGenerator.XMLPosition;



@Component
@ManagedBean
@RequestScoped
public class RentalBean {
    private List<Car> cars;
    private List<Client> clients;
    private List<Rental> rentals;
    
    private Integer rentalId;
    private Integer clientId;
    private Integer carId;
    private Date startDate;
    private Date endDate;
	
	private String report;
    
	
    @Autowired
    RentalDb rentalDb;
    
    @Autowired
    ClientBean clientBean;
    
    @Autowired
    CarBean carBean;
    public List<Rental> getRentals(){
        try {
            return rentalDb.getRental();
        } catch (Exception ex) {
            Logger.getLogger(RentalBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<Rental>();
    }
    
    public String removeRental(Integer id){
        try {
            rentalDb.deleteRental(id);
        } catch (Exception ex) {
            Logger.getLogger(RentalBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public String addRental(){
        try {
            if(rentalId != null && rentalId > 0){
                rentalDb.updateRental(rentalId,getClientId(), getCarId(), getStartDate(), getEndDate());
            }
            else{
                rentalDb.saveRental(getClientId(), getCarId(), getStartDate(), getEndDate());
            }
            
            
        } catch (Exception ex) {
            Logger.getLogger(RentalBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void updateRental(int id){
        this.rentalId = id;
        for(Rental r : getRentals()){
            if(r.getId() == id){
                this.carId = r.getCar().getId();
                this.clientId = r.getClient().getId();
                this.startDate = r.getStartDate();
                this.endDate = r.getEndDate();
            }
        }
    }
	
	public void sendNotification(Rental r){
        com.grandrentauto.email.EmailSenderFactory.createEmailSender("grand.rent.auto@gmail.com", "Gr4ndR3nt4ut0").sendEmail("grand.rent.auto@gmail.com", "grand.rent.auto@gmail.com", "Potwierdzenie wynajmu", r.toString());
    }
	
	public String generateReport() throws Exception{
		xmlGenerator.XMLGenerator generator = new XMLGenerator();
            
		XMLDocument document = new XMLDocument();
        for(Rental r : getRentals()){
            XMLElement el1 = new XMLElement();
            el1.setName("Klient");
            el1.setValue(r.getClient().getFirstName() + " " + r.getClient().getLastName());
            XMLElement el2 = new XMLElement();
			el2.setName("Auto");
            el2.setValue(r.getCar().getModel() + ", " + r.getCar().getRegistrationNumber());
             
			XMLElement el3 = new XMLElement();
            el3.setName("Przedzial czasu");
            el3.setValue(r.getStartDate() + ":" + r.getEndDate());
			
			XMLPosition position = new XMLPosition();
            position.getElementList().add(el1);
            position.getElementList().add(el2);
            position.getElementList().add(el3);
            document.getPositions().add(position);
        }
                
        generator.save(document);
                
        BufferedReader reader = new BufferedReader( new FileReader ("file.xml"));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");
		
        while((line = reader.readLine()) != null) {
            stringBuilder.append( line );
            stringBuilder.append( ls );
		}
            
		String text =  stringBuilder.toString();
        report = text;

		return null;
    }
    
    /**
     * @return the cars
     */
    public List<Car> getCars() {
        return carBean.getCars();
    }

    /**
     * @param cars the cars to set
     */
    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    /**
     * @return the clients
     */
    public List<Client> getClients() {
        return clientBean.getClients();
    }

    /**
     * @param clients the clients to set
     */
    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

   

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @param rentals the rentals to set
     */
    public void setRentals(List<Rental> rentals) {
        this.rentals = rentals;
    }

    /**
     * @return the clientId
     */
    public Integer getClientId() {
        return clientId;
    }

    /**
     * @param clientId the clientId to set
     */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * @return the carId
     */
    public Integer getCarId() {
        return carId;
    }

    /**
     * @param carId the carId to set
     */
    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    /**
     * @return the rentalId
     */
    public Integer getRentalId() {
        return rentalId;
    }

    /**
     * @param rentalId the rentalId to set
     */
    public void setRentalId(Integer rentalId) {
        this.rentalId = rentalId;
    }

    /**
     * @return the report
     */
    public String getReport() {
        return report;
    }

    /**
     * @param report the report to set
     */
    public void setReport(String report) {
        this.report = report;
    }
}
