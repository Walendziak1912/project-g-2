package com.grandrentauto.webbeans;

import com.grandrentauto.data.Car;
import com.grandrentauto.db.CarDb;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@ManagedBean
@RequestScoped
public class CarBean {
    private Integer selectedId;
    
    
    private String carName;
    private String carId;
    
    private Integer idToEdit;
    
    @Autowired
    CarDb carDb;
    public String addCar(){
        Car car = new Car(idToEdit, getCarName(), getCarId());
        try{
            if(idToEdit == null || idToEdit ==0){
            carDb.saveCar(car);
            }
            else{
                carDb.updateCar(car);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
    
    public List<Car> getCars(){
        return carDb.getCars();
       
    }
    
    
    public String removeCar(String id) throws Exception{
        Integer selId = getSelectedId();
		carDb.deleteCar(Integer.parseInt(id));
        return "samochody";
    }

    public String editAction(Car car) {
        this.setCarName(car.getModel());
        this.setCarId(car.getRegistrationNumber());
        this.idToEdit = car.getId();
                car.setId(car.getId()+1);
		car.setIsEditable(true);
		return null;
	}
    
    /**
     * @return the selectedId
     */
    public Integer getSelectedId() {
        return selectedId;
    }

    /**
     * @param selectedId the selectedId to set
     */
    public void setSelectedId(Integer selectedId) {
        this.selectedId = selectedId;
    }

    /**
     * @return the carName
     */
    public String getCarName() {
        return carName;
    }

    /**
     * @param carName the carName to set
     */
    public void setCarName(String carName) {
        this.carName = carName;
    }

    /**
     * @return the carId
     */
    public String getCarId() {
        return carId;
    }

    /**
     * @param carId the carId to set
     */
    public void setCarId(String carId) {
        this.carId = carId;
    }

    /**
     * @return the idToEdit
     */
    public Integer getIdToEdit() {
        return idToEdit;
    }

    /**
     * @param idToEdit the idToEdit to set
     */
    public void setIdToEdit(Integer idToEdit) {
        this.idToEdit = idToEdit;
    }
}
