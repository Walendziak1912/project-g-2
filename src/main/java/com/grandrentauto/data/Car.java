package com.grandrentauto.data;

import java.io.Serializable;


public class Car {
    private String model;
    private Integer id;
    private String registrationNumber;

    
    private Boolean isEditable;
    
    public Car(Integer id, String model, String registrationNumber){
        this.id = id;
        this.model = model;
        this.registrationNumber = registrationNumber;
    }
    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the registrationNumber
     */
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    /**
     * @param registrationNumber the registrationNumber to set
     */
    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    /**
     * @return the isEditable
     */
    public Boolean getIsEditable() {
        return isEditable;
    }

    /**
     * @param isEditable the isEditable to set
     */
    public void setIsEditable(Boolean isEditable) {
        this.isEditable = isEditable;
    }
}
